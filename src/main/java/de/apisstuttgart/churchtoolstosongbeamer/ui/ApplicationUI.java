package de.apisstuttgart.churchtoolstosongbeamer.ui;

import de.apisstuttgart.churchtoolstosongbeamer.domain.CtEvent;
import de.apisstuttgart.churchtoolstosongbeamer.service.AgendaConverterService;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import javax.swing.*;
import java.awt.*;
import java.net.URI;


@Service
@CommonsLog
public class ApplicationUI {

    protected Container contentPanel;

    protected JLabel messageLabel;


    private ApplicationUI() {
        JFrame jFrame = new JFrame("Churchtools to Songbeamer");
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        contentPanel = jFrame.getContentPane();
        contentPanel.setLayout(new BorderLayout());

        JPanel messagePanel = new JPanel();
        messageLabel = new JLabel();
        messagePanel.add(messageLabel);
        contentPanel.add(messagePanel, BorderLayout.NORTH);

        jFrame.setSize(500, 500);
        jFrame.setVisible(true);

        showMessage("Bitte warten, die Daten werden geladen.");
    }


    private void repaintUI() {
        contentPanel.revalidate();
        contentPanel.repaint();
    }


    public void showMessage(String message) {
        messageLabel.setText(message);
    }


    public void showUpdate(String url) {
        JPanel downloadPanel = new JPanel();

        downloadPanel.add(new JLabel("Ein Update ist verfügbar:"));

        JButton downloadButton = new JButton("Jetzt downloaden!");
        downloadButton.addActionListener(actionEvent -> {
            try {
                Desktop.getDesktop().browse(URI.create(url));
            } catch (Exception e) {
                log.error("Could not open browser with update URL");
            }
        });

        downloadPanel.add(downloadButton);

        contentPanel.add(downloadPanel, BorderLayout.SOUTH);
        repaintUI();
    }


    public void showEventList(CtEvent[] ctEvents, AgendaConverterService agendaConverterService) {
        showMessage("Ablauf auswählen um diesen in SongBeamer zu öffnen.");

        JList list = new JList(ctEvents);
        list.setFixedCellHeight(25);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.addListSelectionListener(selectionEvent -> {
            if (!selectionEvent.getValueIsAdjusting()) {
                log.info("Selected object for translation is: " + list.getSelectedValue());
                agendaConverterService.translate((CtEvent) list.getSelectedValue());
            }
        });

        contentPanel.add(list);
        repaintUI();
    }
}
