package de.apisstuttgart.churchtoolstosongbeamer.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;


@Getter
@Setter
public class CtSong {

    protected String id;

    @JsonProperty("bezeichnung")
    protected String name;

    protected String author;

    @JsonProperty("songcategory_id")
    protected String categoryId;

    @JsonProperty("arrangement")
    protected Map<Integer, CtSongArrangement> arrangements;


    public Collection<CtSongArrangement> getArrangements() {
        if (arrangements == null) {
            return null;
        }
        return arrangements.values();
    }

    public CtSongArrangement getArrangement(int id) {
        if (arrangements == null) {
            return null;
        }
        return arrangements.getOrDefault(id, null);
    }
}
