package de.apisstuttgart.churchtoolstosongbeamer.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;


@Getter
@Setter
public class CtAgendaItem implements Comparable {

    protected String id;

    @JsonProperty("bezeichnung")
    protected String name;

    @JsonProperty("category_id")
    protected String categoryId;

    @JsonProperty("arrangement_id")
    protected String arrangementId;

    @JsonProperty("header_yn")
    protected String titleFlag;

    @JsonProperty("preservice_yn")
    protected String preService;

    protected String responsible;

    protected String duration;

    protected String note;

    @JsonProperty("sortkey")
    protected String sortKey;

    @JsonProperty("servicegroup")
    protected Map<Integer, String> serviceGroupNotes;


    public boolean isPreService() {
        return "1".equals(preService);
    }

    public boolean isTitle() {
        return "1".equals(titleFlag);
    }

    public boolean isSong() {
        return name.isEmpty() && arrangementId != null && !arrangementId.isEmpty();
    }

    public int getSortKey() {
        return Integer.parseInt(sortKey);
    }

    public String getDuration() {
        try {
            int duration = Integer.parseInt(this.duration);
            int minutes = duration / 60;
            int seconds = duration % 60;

            return minutes + ":" + (seconds < 10 ? "0" : "") + seconds;
        } catch (Exception e) {
        }
        return null;
    }


    @Override
    public int compareTo(Object otherObject) {
        if (otherObject instanceof CtAgendaItem) {
            return getSortKey() - ((CtAgendaItem) otherObject).getSortKey();
        }

        return 0;
    }

    @Override
    public String toString() {
        String duration = getDuration();

        if (isSong()) {
            return "Song: " + arrangementId + " - " + getDuration();
        }
        if (isTitle() || duration == null) {
            return name;
        }
        return name + " - " + getDuration();
    }
}
