package de.apisstuttgart.churchtoolstosongbeamer.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CtCsrfToken {

    @JsonProperty("data")
    protected String token;
}
