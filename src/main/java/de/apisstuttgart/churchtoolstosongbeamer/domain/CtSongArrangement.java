package de.apisstuttgart.churchtoolstosongbeamer.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;


@Getter
@Setter
public class CtSongArrangement {

    protected String id;

    @JsonProperty("bezeichnung")
    protected String name;

    protected Map<Long, CtFile> files;


    public Collection<CtFile> getFiles() {
        if (files == null) {
            return null;
        }
        return files.values();
    }

    public CtFile getFile(String extension) {
        if (getFiles() == null) {
            return null;
        }

        Optional<CtFile> ctFilePromise = getFiles().stream().filter(file -> file.name.endsWith(extension)).findAny();

        return ctFilePromise.orElse(null);
    }
}
