package de.apisstuttgart.churchtoolstosongbeamer.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString(of = {"name"})
public class CtAgenda {

    protected String id;

    @JsonProperty("bezeichnung")
    protected String name;

    @JsonProperty("category_id")
    protected String categoryId;
}
