package de.apisstuttgart.churchtoolstosongbeamer.domain;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CtResponse<T> {

    protected String status;
    protected T data;

    public boolean isStatusSuccessful() {
        return "success".equals(status);
    }
}
