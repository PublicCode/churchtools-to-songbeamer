package de.apisstuttgart.churchtoolstosongbeamer.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class CtFile {

    protected String id;

    @JsonProperty("bezeichnung")
    protected String name;

    protected String filename;

    @JsonProperty("domain_type")
    protected String domainType;

    @JsonProperty("domain_id")
    protected String domainId;
}
