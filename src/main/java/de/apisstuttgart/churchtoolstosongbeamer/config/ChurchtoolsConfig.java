package de.apisstuttgart.churchtoolstosongbeamer.config;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties("churchtools")
@Getter
@Setter
@CommonsLog
public class ChurchtoolsConfig {

    protected String instance;
    protected String userId;
    protected String userToken;

    private String getInstanceUrl() {
        if (instance == null || instance.isEmpty()) {
            log.error("Please set the churchtools instance");
            return null;
        }

        if (instance.contains(".")) {
            return "https://" + instance + "/";
        }

        return "https://" + instance + ".church.tools/";
    }

    public String getUrlBase() {
        String instanceUrl = getInstanceUrl();
        return instanceUrl != null ? instanceUrl + "index.php" : "";
    }

    public String getApiBase() {
        String instanceUrl = getInstanceUrl();
        return instanceUrl != null ? instanceUrl + "api/" : "";
    }
}
