package de.apisstuttgart.churchtoolstosongbeamer.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties("songbeamer")
@Getter
@Setter
public class SongbeamerConfig {

    protected String agendaExtension;
    protected String songsExtension;

    protected String installPath;
    protected String executableName;

    protected String agendaBaseFolder;
    protected String songsBaseFolder;
    protected String songsSubFolder;
}
