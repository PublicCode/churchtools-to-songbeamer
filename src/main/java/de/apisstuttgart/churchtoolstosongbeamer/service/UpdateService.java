package de.apisstuttgart.churchtoolstosongbeamer.service;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
@CommonsLog
public class UpdateService {

    @Value("${application.version}")
    protected String version;

    @Value("${application.update-url}")
    protected String updateUrl;

    @Value("${application.download-url}")
    protected String downloadUrl;

    protected RestTemplate restTemplate = new RestTemplate();


    public boolean isUpdateAvailable() {
        try {
            String response = restTemplate.getForObject(getUpdateUrl(), String.class);

            Matcher matcher = Pattern.compile("<version>(.+?)</version>", Pattern.DOTALL).matcher(response);
            if (matcher.find()) {
                String responseVersion = matcher.group(1);
                if (version != null && !version.equals(responseVersion)) {
                    log.warn("Update is available");
                    return true;
                } else {
                    log.info("No update found");
                }
            }

        } catch (HttpClientErrorException e) {
            log.warn("Update check failed. " + e.getMessage());
        }
        return false;
    }

    private String getUpdateUrl() {
        return updateUrl + "?version=" + version;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }
}
