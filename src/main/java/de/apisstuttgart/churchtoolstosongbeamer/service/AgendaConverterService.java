package de.apisstuttgart.churchtoolstosongbeamer.service;

import de.apisstuttgart.churchtoolstosongbeamer.config.SongbeamerConfig;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtAgendaItem;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtEvent;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Service
@CommonsLog
public class AgendaConverterService {

    @Autowired
    protected CtAgendaService agendaService;

    @Autowired
    protected CtSongService songService;

    @Autowired
    protected SongbeamerConfig songbeamerConfig;

    @Autowired
    protected SbControllerService sbControllerService;

    @Autowired
    protected FileWriterService fileWriterService;


    protected static String LINE_SEPARATOR = System.getProperty("line.separator");


    public void translate(CtEvent ctEvent) {
        CtAgendaItem[] agendaItems = agendaService.getAgendaItems(ctEvent.getId());


        StringBuilder sbAgenda = new StringBuilder();
        sbAgenda.append(getSbPrefix());

        for (CtAgendaItem item : agendaItems) {
            if (item.isSong()) {
                CtSongService.SongInfo songInfo = songService.getSong(item.getArrangementId());

                if (songInfo != null) {
                    sbAgenda.append(getSbSong(songInfo.getName() + " - " + item.getDuration(), songInfo.getFilename()));
                } else {
                    sbAgenda.append(getSbSong(item.toString(), ""));
                }

            } else if (item.isTitle()) {
                sbAgenda.append(getSbTitle(item.toString()));
            } else {
                sbAgenda.append(getSbNote(item.toString()));
            }
        }

        sbAgenda.append(getSbPostfix());


        Path sbAgendaFile = Paths.get(songbeamerConfig.getAgendaBaseFolder(), sanitizeFilename(ctEvent.toString()) + songbeamerConfig.getAgendaExtension());

        try {
            if (!sbAgendaFile.getParent().toFile().exists()) {
                Files.createDirectories(sbAgendaFile.getParent());
            }

            fileWriterService.write(sbAgendaFile, sbAgenda.toString(), true);
            log.info("Songbeamer agenda was written to " + sbAgendaFile.toAbsolutePath().toString());


            sbControllerService.stopSongBeamer();
            sbControllerService.startSongBeamer(sbAgendaFile.toAbsolutePath().toString());

        } catch (IOException e) {
            log.error("Unable to write songbeamer agenda to " + sbAgendaFile.toAbsolutePath().toString(), e);
        }
    }

    private String getSbPrefix() {
        return "object AblaufPlanItems : TAblaufPlanItems" + LINE_SEPARATOR +
                "  items = <";
    }

    private String getSbPostfix() {
        return ">" + LINE_SEPARATOR +
                "end";
    }

    private String getSbSong(String name, String filename) {
        return getSbItem(name, "clBlue", filename);
    }

    private String getSbTitle(String name) {
        return getSbItem(name, "clRed", null);
    }

    private String getSbNote(String name) {
        return getSbItem(name, "clBlack", null);
    }

    private String getSbItem(String name, String color, String filename) {
        return LINE_SEPARATOR +
                "    item" + LINE_SEPARATOR +
                "      Caption = '" + name + "'" + LINE_SEPARATOR +
                (filename != null ? "      FileName = '" + filename + "'" + LINE_SEPARATOR : "") +
                "      Color = " + color + LINE_SEPARATOR +
                "    end";
    }

    private String sanitizeFilename(String filename) {
        return filename.replaceAll("[\\/:*?<>|]+", "-").trim();
    }
}
