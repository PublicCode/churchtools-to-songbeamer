package de.apisstuttgart.churchtoolstosongbeamer.service;

import de.apisstuttgart.churchtoolstosongbeamer.config.ChurchtoolsConfig;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtAgendaItem;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtCsrfToken;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtResponse;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;


@Service
@CommonsLog
public class CtLoginService {

    @Autowired
    protected ChurchtoolsConfig churchtoolsConfig;

    protected RestTemplate restTemplate = new RestTemplate();

    protected static HttpHeaders requestHeader = new HttpHeaders();


    /**
     * Login with id and token from the settings
     */
    protected void performLogin() {

        String requestParams = "?q=login/ajax"
                + "&func=loginWithToken"
                + "&id=" + churchtoolsConfig.getUserId()
                + "&token=" + churchtoolsConfig.getUserToken();

        ResponseEntity<CtResponse> response = restTemplate.postForEntity(churchtoolsConfig.getUrlBase() + requestParams, null, CtResponse.class);

        if (response.getStatusCode() == HttpStatus.OK && response.getBody().isStatusSuccessful()) {
            log.info("Login was successful :)");

            List<String> cookies = response.getHeaders().get(HttpHeaders.SET_COOKIE);

            StringBuilder cookieBuilder = new StringBuilder();

            cookies.forEach(cookie -> {
                cookieBuilder.append(cookie.replaceFirst(";.*$", "; "));
            });

            requestHeader.set(HttpHeaders.COOKIE, cookieBuilder.toString());
        } else {
            log.error("Login was not possible, please check your instance, id and token.");
        }
    }


    /**
     * Request CSRF token for further requests
     */
    protected void requestCsrfToken() {
        HttpEntity requestEntity = new HttpEntity(requestHeader);

        ResponseEntity<CtCsrfToken> response = restTemplate.exchange(churchtoolsConfig.getApiBase() + "csrftoken", HttpMethod.GET, requestEntity, CtCsrfToken.class);

        if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null && response.getBody().getToken() != null) {
            log.info("Got CSRF token from CT");

            requestHeader.set("CSRF-Token", response.getBody().getToken());
        } else {
            log.error("Failed to load CSRF token from CT.");
        }
    }


    /**
     * Get request header which handles the authentication
     *
     * @return
     */
    public HttpHeaders getRequestHeader() {
        if (requestHeader.isEmpty()) {
            this.performLogin();
            this.requestCsrfToken();
        }
        return requestHeader;
    }
}
