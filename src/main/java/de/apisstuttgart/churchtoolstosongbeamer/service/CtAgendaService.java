package de.apisstuttgart.churchtoolstosongbeamer.service;

import de.apisstuttgart.churchtoolstosongbeamer.config.ChurchtoolsConfig;
import de.apisstuttgart.churchtoolstosongbeamer.config.SongbeamerConfig;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtAgenda;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtAgendaItem;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtResponse;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@CommonsLog
public class CtAgendaService {

    @Autowired
    protected CtLoginService ctLoginService;

    @Autowired
    protected ChurchtoolsConfig churchtoolsConfig;

    @Autowired
    protected SongbeamerConfig songbeamerConfig;

    protected RestTemplate restTemplate = new RestTemplate();


    public CtAgenda getAgenda(String eventId) {

        String requestParams = "?q=churchservice/ajax&func=loadAgendaForEvent&event_id=" + eventId;

        HttpEntity requestEntity = new HttpEntity(ctLoginService.getRequestHeader());

        ResponseEntity<CtResponse<CtAgenda>> response = restTemplate.exchange(churchtoolsConfig.getUrlBase() + requestParams, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<CtResponse<CtAgenda>>() {
        });

        if (response.getStatusCode() == HttpStatus.OK && response.getBody().isStatusSuccessful()) {
            log.info("Successful agenda lookup " + response.getBody().getData());

            return response.getBody().getData();
        }

        log.error("Could not fetch agenda object for event " + eventId);
        return null;
    }


    public CtAgendaItem[] getAgendaItems(String eventId) {

        CtAgenda ctAgenda = getAgenda(eventId);

        String requestParams = "?q=churchservice/ajax&func=loadAgendaItems&agenda_id=" + ctAgenda.getId();

        HttpEntity requestEntity = new HttpEntity(ctLoginService.getRequestHeader());

        ResponseEntity<CtResponse<Map<Long, CtAgendaItem>>> response = restTemplate.exchange(churchtoolsConfig.getUrlBase() + requestParams, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<CtResponse<Map<Long, CtAgendaItem>>>() {
        });

        if (response.getStatusCode() == HttpStatus.OK && response.getBody().isStatusSuccessful()) {
            log.info("Successful agenda item lookup");

            Collection<CtAgendaItem> itemCollection = response.getBody().getData().values();

            itemCollection = itemCollection.stream().filter(item -> !item.isPreService()).collect(Collectors.toList());

            CtAgendaItem[] items = itemCollection.toArray(new CtAgendaItem[0]);

            Arrays.sort(items);

            log.debug("Agenda: " + Arrays.toString(items));

            return items;
        }

        log.error("Login was not possible, please check your instance, id and token.");
        return null;
    }
}
