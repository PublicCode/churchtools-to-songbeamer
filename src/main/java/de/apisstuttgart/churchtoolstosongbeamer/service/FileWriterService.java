package de.apisstuttgart.churchtoolstosongbeamer.service;

import lombok.extern.apachecommons.CommonsLog;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;


@Service
@CommonsLog
public class FileWriterService {


    public void write(Path path, String content) throws IOException {
        write(path, content, false);
    }


    public void write(Path path, String content, boolean writeUTF8BOM) throws IOException {
        if (writeUTF8BOM) {
            content = "\uFEFF" + content;
        }

        try {
            Files.write(path, content.getBytes(Charset.forName("UTF-8")));
        } catch (IOException e) {
            log.error("Could not write content to " + path);
            throw e;
        }
    }
}
