package de.apisstuttgart.churchtoolstosongbeamer.service;

import de.apisstuttgart.churchtoolstosongbeamer.config.ChurchtoolsConfig;
import de.apisstuttgart.churchtoolstosongbeamer.config.SongbeamerConfig;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtFile;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtResponse;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtSong;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtSongArrangement;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@CommonsLog
public class CtSongService {

    @Autowired
    protected CtLoginService ctLoginService;

    @Autowired
    protected ChurchtoolsConfig churchtoolsConfig;

    @Autowired
    protected SongbeamerConfig songbeamerConfig;

    @Autowired
    protected FileWriterService fileWriterService;

    protected RestTemplate restTemplate = new RestTemplate();

    protected static Collection<CtSong> allSongs;


    @Getter
    @Setter
    @AllArgsConstructor
    public static class SongInfo {

        protected String name;
        protected String filename;
    }


    public SongInfo getSong(String arrangementId) {
        this.getAllSongs();

        int id = Integer.parseInt(arrangementId);

        List<CtSong> matchingSongs = allSongs.stream().filter(song -> song.getArrangement(id) != null).collect(Collectors.toList());

        if (!matchingSongs.isEmpty()) {
            CtSongArrangement arrangement = matchingSongs.get(0).getArrangement(id);
            CtFile ctFile = arrangement.getFile(songbeamerConfig.getSongsExtension());

            if (ctFile != null) {

                Path relativePath = Paths.get(songbeamerConfig.getSongsSubFolder(), ctFile.getName());
                Path absolutePath = Paths.get(songbeamerConfig.getSongsBaseFolder(), relativePath.toString());

                try {
                    if (!absolutePath.getParent().toFile().exists()) {
                        Files.createDirectories(absolutePath.getParent());
                    }

                    this.downloadSong(ctFile, absolutePath);

                    return new SongInfo(matchingSongs.get(0).getName(), relativePath.toString());

                } catch (IOException e) {
                    log.error("I/O error while downloading song " + ctFile.getName());
                }
            }
        }

        log.error("Song download failed for Arrangement " + arrangementId);
        return null;
    }


    protected void downloadSong(CtFile ctFile, Path target) throws IOException {
        String requestParams = "?q=churchservice/filedownload&id=" + ctFile.getId() + "&filename=" + ctFile.getFilename();

        HttpEntity requestEntity = new HttpEntity(ctLoginService.getRequestHeader());

        ResponseEntity<String> response = restTemplate.exchange(churchtoolsConfig.getUrlBase() + requestParams, HttpMethod.POST, requestEntity, String.class);

        // CT does not respond with 404 if file is missing, test against a html file starting wih doctype.
        if (response.getStatusCode() == HttpStatus.OK && !response.getBody().startsWith("<!DOCTYPE html>")) {
            log.info("Successful song lookup");

            String song = response.getBody();

            fileWriterService.write(target, song);

            log.debug("Song: " + song);

        } else {
            log.error("Error while fetching all songs.");
        }
    }


    public void getAllSongs() {
        if (allSongs != null) {
            return;
        }

        String requestParams = "?q=churchservice/ajax&func=getAllSongs";

        HttpEntity requestEntity = new HttpEntity(ctLoginService.getRequestHeader());

        ResponseEntity<CtResponse<Map<String, Map<Long, CtSong>>>> response = restTemplate.exchange(churchtoolsConfig.getUrlBase() + requestParams, HttpMethod.POST, requestEntity, new ParameterizedTypeReference<CtResponse<Map<String, Map<Long, CtSong>>>>() {
        });

        if (response.getStatusCode() == HttpStatus.OK && response.getBody().isStatusSuccessful()) {
            log.info("Successful song lookup");

            allSongs = response.getBody().getData().get("songs").values();

            log.debug("Songs: " + allSongs);

        } else {
            log.error("Error while fetching all songs.");
        }
    }
}
