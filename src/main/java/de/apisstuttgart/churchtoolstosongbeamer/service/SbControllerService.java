package de.apisstuttgart.churchtoolstosongbeamer.service;

import de.apisstuttgart.churchtoolstosongbeamer.config.SongbeamerConfig;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
@CommonsLog
public class SbControllerService {

    protected static String FILE_SEPARATOR = System.getProperty("file.separator");

    @Autowired
    protected SongbeamerConfig songbeamerConfig;

    public void startSongBeamer(String agendaPath) {
        String executablePath = appendPathSeparator(songbeamerConfig.getInstallPath()) + songbeamerConfig.getExecutableName();

        runCommand("\"" + executablePath + "\" \"" + agendaPath + "\"");
    }

    public void stopSongBeamer() {
        runCommand("taskkill /t /IM SongBeamer.exe");
    }

    private void runCommand(String command) {
        try {
            log.info("Run command: " + command);
            Runtime.getRuntime().exec(command);
            Thread.sleep(1000);
        } catch (IOException | InterruptedException e) {
            log.error("Could not run command: " + command);
        }
    }

    private String appendPathSeparator(String string) {
        return string + (string != null && !string.endsWith(FILE_SEPARATOR) ? FILE_SEPARATOR : "");
    }
}
