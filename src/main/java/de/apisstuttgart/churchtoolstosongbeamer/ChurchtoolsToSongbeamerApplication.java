package de.apisstuttgart.churchtoolstosongbeamer;

import de.apisstuttgart.churchtoolstosongbeamer.config.ChurchtoolsConfig;
import de.apisstuttgart.churchtoolstosongbeamer.domain.CtEvent;
import de.apisstuttgart.churchtoolstosongbeamer.service.AgendaConverterService;
import de.apisstuttgart.churchtoolstosongbeamer.service.CtEventService;
import de.apisstuttgart.churchtoolstosongbeamer.service.UpdateService;
import de.apisstuttgart.churchtoolstosongbeamer.ui.ApplicationUI;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;


@SpringBootApplication
@CommonsLog
public class ChurchtoolsToSongbeamerApplication {

    @Autowired
    protected AgendaConverterService agendaConverterService;

    @Autowired
    protected CtEventService ctEventService;

    @Autowired
    protected UpdateService updateService;

    @Autowired
    protected ChurchtoolsConfig churchtoolsConfig;

    @Autowired
    protected ApplicationUI appUI;


    public static void main(String[] args) {
        new SpringApplicationBuilder(ChurchtoolsToSongbeamerApplication.class).headless(false).run(args);
    }


    @Bean
    public CommandLineRunner run() {
        return args -> {
            log.debug("Debug logging is active.");

            if (updateService.isUpdateAvailable()) {
                appUI.showUpdate(updateService.getDownloadUrl());
            }

            // Check config
            if (churchtoolsConfig.getUrlBase().isEmpty()) {
                appUI.showMessage("Bitte Churchtools Instanz richtig konfigurieren.");
            } else if (churchtoolsConfig.getUserId().isEmpty()) {
                appUI.showMessage("Bitte die User ID richtig konfigurieren.");
            } else if (churchtoolsConfig.getUserToken().isEmpty()) {
                appUI.showMessage("Bitte die User Token richtig konfigurieren.");

                // Config seems correct
            } else {
                try {
                    CtEvent[] ctEvents = ctEventService.getAllEvents();
                    if (ctEvents != null) {
                        appUI.showEventList(ctEvents, agendaConverterService);
                    } else {
                        appUI.showMessage("Daten konnten nicht geladen werden.");
                    }
                } catch (Exception e) {
                    log.error("Error " + e.getMessage());

                    if (e instanceof HttpStatusCodeException) {
                        switch (((HttpStatusCodeException) e).getStatusCode()) {

                            case UNAUTHORIZED:
                                appUI.showMessage("Die Logindaten sind nicht korrekt.");
                                break;
                            case INTERNAL_SERVER_ERROR:
                                appUI.showMessage("Serverfehler, versuche es später wieder.");
                                break;

                            default:
                                appUI.showMessage("Kommunikationsfehler " + ((HttpStatusCodeException) e).getRawStatusCode() + ", versuche es später wieder.");
                        }
                    } else {
                        appUI.showMessage("Undefinierter Fehler, versuche es später wieder.");
                        throw e;
                    }
                }
            }
        };
    }
}
