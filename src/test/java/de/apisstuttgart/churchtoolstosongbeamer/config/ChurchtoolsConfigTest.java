package de.apisstuttgart.churchtoolstosongbeamer.config;

import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class ChurchtoolsConfigTest {

    @Test
    public void testUnsetInstance() {
        ChurchtoolsConfig config = new ChurchtoolsConfig();
        assertEquals("", config.getUrlBase());
    }

    @Test
    public void testChurchToolsSaaSInstance() {
        ChurchtoolsConfig config = new ChurchtoolsConfig();
        config.setInstance("test");
        assertEquals("https://test.church.tools/index.php", config.getUrlBase());
    }

    @Test
    public void testSelfhosterInstance() {
        ChurchtoolsConfig config = new ChurchtoolsConfig();
        config.setInstance("example.com");
        assertEquals("https://example.com/index.php", config.getUrlBase());
    }
}
