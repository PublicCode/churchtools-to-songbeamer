package de.apisstuttgart.churchtoolstosongbeamer.domain;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CtAgendaItemTest {

    @Test
    public void testGetDuration() {
        CtAgendaItem ctAgendaItem = new CtAgendaItem();
        ctAgendaItem.setDuration("1234");
        assertEquals("20:34", ctAgendaItem.getDuration());
    }
}
