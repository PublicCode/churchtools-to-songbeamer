# Churchtools to Songbeamer

1. [Software](https://gitlab.com/PublicCode/churchtools-to-songbeamer/-/jobs/artifacts/release/download?job=job) herunterladen
1. [Yml](https://gitlab.com/PublicCode/churchtools-to-songbeamer/raw/release/docs/application.yml?inline=false)-Datei anpassen
1. Jar-Datei starten
1. Passenden Ablauf doppelklicken
1. Ablauf in Songbeamer öffnen

Details zur [Konfiguration](docs/config.md)
